/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

abstract class BaseVideoManager {

    protected var adEventListener: AdvertisementEventListener? = null
    protected var isVideoRewardDefault = false
    protected var hasAutoLoad = true

    fun setEventListener(listener: AdvertisementEventListener?) {
        adEventListener = listener
    }

    /**
     * Auto-upload new video after closing the current video.
     * Default enabled.
     *
     * @false - disable
     * @true - enable
     */
    fun setAutoLoad(flag: Boolean) {
        hasAutoLoad = flag
    }

    open fun useInterstitialAndReward() {
        isVideoRewardDefault = true
    }

    open fun setRewardVideoAsDefault() {
        isVideoRewardDefault = true
    }

    open fun setInterstitialVideoAsDefault() {
        isVideoRewardDefault = false
    }

    /**
     * @return true - if the video is uploaded and ready to be shown.
     */
    abstract fun isReadyToDisplay(isReward: Boolean): Boolean

    /**
     * Set the user ID at the runtime.
     * @param [userID] - user ID.
     */
    abstract fun setUserId(userID: String)
}