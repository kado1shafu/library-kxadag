/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

interface AdvertisementEventListener {
    fun videoIsNotReady(isReward: Boolean)

    fun videoStartedLoading(isReward: Boolean)

    /**
     * You must call this function,
     * if an error occurred while displaying the advertisement.
     */
    fun videoNotDisplayed(error: String, isReward: Boolean)

    fun onVideoLoaded(isReward: Boolean)

    fun onVideoNotLoaded(error: String, isReward: Boolean)
    /**
     * You must call this function when the ad opened.
     * You can reset UI to reflect state change.
     */
    fun onVideoOpened(isReward: Boolean)

    /**
     * You must call this function when the ad has been closed.
     * You can reset UI to reflect state change, show dialog or smth.
     * Flag [hasRewarded] indicates whether the user was rewarded.
     */
    fun onVideoClosed(hasRewarded: Boolean)

    /**
     * You must call this function when the ad sdk rewards the user.
     * You can update the user's balance in the DB or server.
     */
    fun rewardUser()
}
