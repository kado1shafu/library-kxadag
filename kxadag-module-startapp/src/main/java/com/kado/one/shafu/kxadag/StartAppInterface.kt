/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.app.Activity

interface StartAppInterface {

    fun showStartAppVideo(isReward: Boolean)
    fun setStartAppEventListener(listener: AdvertisementEventListener?)

    fun onPause(activity: Activity)
    fun onResume(activity: Activity)
    fun onBackPressed(): Boolean
}