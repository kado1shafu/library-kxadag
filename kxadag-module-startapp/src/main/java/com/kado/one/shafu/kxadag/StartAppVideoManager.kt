/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.app.Activity
import android.content.Context
import android.util.Log
import com.startapp.android.publish.adsCommon.Ad
import com.startapp.android.publish.adsCommon.StartAppAd
import com.startapp.android.publish.adsCommon.StartAppSDK
import com.startapp.android.publish.adsCommon.VideoListener
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener
import javax.inject.Inject

class StartAppVideoManager @Inject constructor() : BaseVideoManager() {

    private val TAG = "StartAppVideoManager"

    private var isVerified = false

    private var interstitial: StartAppAd? = null
    private var reward: StartAppAd? = null

    private lateinit var defaultInterstitialLoadListener: AdEventListener
    private lateinit var defaultInterstitialDisplayListener: AdDisplayListener

    private lateinit var defaultRewardedListener: VideoListener
    private lateinit var defaultRewardedLoadListener: AdEventListener
    private lateinit var defaultRewardedDisplayListener: AdDisplayListener

    init {
        Log.d(TAG, "Init")
    }

    //In your main activity, go to the OnCreate method and before calling setContentView()
    // call StartAppVideoManager.setupSdk(...)
    fun setupSdk(activity: Activity, appId: String) {
        StartAppSDK.init(activity, appId, false)
        StartAppAd.disableSplash()
        StartAppAd.disableAutoInterstitial()
        StartAppSDK.setUserConsent(activity, "pas", System.currentTimeMillis(), false)
    }

    fun useInterstitial(context: Context) {
        interstitial = StartAppAd(context)
        defaultInterstitialLoadListener = createInterstitialLoadListener()
        defaultInterstitialDisplayListener = createInterstitialDisplayListener()
    }

    fun userRewardedVideo(context: Context) {
        defaultRewardedListener = VideoListener {
            isVerified = true
            adEventListener?.rewardUser()
        }

        val ad = StartAppAd(context).apply {
            setVideoListener(defaultRewardedListener)
        }
        reward = ad

        defaultRewardedLoadListener = createRewardedLoadListener()
        defaultRewardedDisplayListener = createRewardedDisplayListener()
    }

    fun loadVideo(isReward: Boolean = isVideoRewardDefault) {
        if (!isReadyToDisplay(isReward)) {
            if (isReward) {
                Log.d(TAG, "[loadVideo] Load rewarded video")
                adEventListener?.videoStartedLoading(isReward)
                interstitial?.loadAd(StartAppAd.AdMode.REWARDED_VIDEO, defaultRewardedLoadListener)
            } else {
                Log.d(TAG, "[loadVideo] Load interstitial ad")
                adEventListener?.videoStartedLoading(isReward)
                interstitial?.loadAd(defaultInterstitialLoadListener)
            }
        }
    }

    fun showVideo(isReward: Boolean = isVideoRewardDefault) {
        if (isReadyToDisplay(isReward)) {
            if (isReward) {
                isVerified = false
                reward?.showAd(defaultRewardedDisplayListener)
            } else {
                interstitial?.showAd(defaultInterstitialDisplayListener)
            }
        } else {
            Log.w(TAG, "[showVideo] Video is not ready")
            adEventListener?.videoIsNotReady(isReward)
        }
    }

    fun onPause(isReward: Boolean) {
        if (isReward) reward?.onPause()
        else interstitial?.onPause()
    }

    fun onResume(isReward: Boolean) {
        if (isReward) reward?.onResume()
        else interstitial?.onResume()
    }

    fun onBackPressed(isReward: Boolean) {
        if (isReward) reward?.onBackPressed()
        else interstitial?.onBackPressed()
    }

    override fun isReadyToDisplay(isReward: Boolean): Boolean =
        if (isReward) reward?.isReady ?: false
        else interstitial?.isReady ?: false

    override fun setUserId(userID: String) {}

    private fun createInterstitialLoadListener() = object : AdEventListener {
        override fun onFailedToReceiveAd(p0: Ad?) {
            Log.w(TAG, "[onFailedToReceiveAd] Interstitial ad failed to load")
            adEventListener?.onVideoNotLoaded("[onFailedToReceiveAd] Interstitial ad failed to load", false)
        }

        override fun onReceiveAd(p0: Ad?) {
            Log.d(TAG, "[onReceiveAd]  StartApp ad loaded")
            adEventListener?.onVideoLoaded(false)
        }

    }

    private fun createInterstitialDisplayListener() = object : AdDisplayListener {
        override fun adHidden(p0: Ad?) {
            Log.d(TAG, "[adHidden] Ad Dismissed")
            adEventListener?.onVideoClosed(false)
            if (hasAutoLoad) loadVideo(false)
        }

        override fun adDisplayed(p0: Ad?) {
            Log.d(TAG, "[adDisplayed] Ad opened")
            adEventListener?.onVideoOpened(false)
        }

        override fun adNotDisplayed(p0: Ad?) {
            Log.w(TAG, "[onError] Play ad error occurred")
            adEventListener?.videoNotDisplayed("Play ad error occurred", false)
        }

        override fun adClicked(p0: Ad?) {
        }
    }


    private fun createRewardedLoadListener() = object : AdEventListener {
        override fun onFailedToReceiveAd(p0: Ad?) {
            Log.w(TAG, "[onFailedToReceiveAd] Rewarded video failed to load")
            adEventListener?.onVideoNotLoaded("[onFailedToReceiveAd] Interstitial ad failed to load", true)
        }

        override fun onReceiveAd(p0: Ad?) {
            Log.d(TAG, "[onReceiveAd]  StartApp ad loaded")
            adEventListener?.onVideoLoaded(true)
        }

    }

    private fun createRewardedDisplayListener() = object : AdDisplayListener {
        override fun adHidden(p0: Ad?) {
            Log.d(TAG, "[adHidden] Ad Dismissed")
            adEventListener?.onVideoClosed(isVerified)
            if (hasAutoLoad) loadVideo(true)
        }

        override fun adDisplayed(p0: Ad?) {
            Log.d(TAG, "[adDisplayed] Ad opened")
            adEventListener?.onVideoOpened(true)
        }

        override fun adNotDisplayed(p0: Ad?) {
            Log.w(TAG, "[onError] Play ad error occurred")
            adEventListener?.videoNotDisplayed("Play ad error occurred", true)
        }

        override fun adClicked(p0: Ad?) {
        }

    }
}