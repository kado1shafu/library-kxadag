# Module StartApp

[ ![Download](https://api.bintray.com/packages/kado1shafu/maven/kxadag-module-startapp/images/download.svg) ](https://bintray.com/kado1shafu/maven/kxadag-module-startapp/_latestVersion)

## Integration

Подключить репозиторий Maven и добавить зависимость в gradle

```groovy
allprojects {
    repositories {
        maven {
            url 'https://dl.bintray.com/kado1shafu/maven'
        }
    }
}

...

dependencies {
    implementation "com.kado.one.shafu.kxadag:kxadag-module-startapp:latestVersion"
}
```

В <manifest> добавить:

```xml
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<!--Необязательно-->
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
<uses-permission android:name="android.permission.BLUETOOTH" />
```
Далее внуть тега <application> добавить:
```xml
<activity android:name="com.startapp.android.publish.ads.list3d.List3DActivity"
          android:theme="@android:style/Theme" />

<activity android:name="com.startapp.android.publish.adsCommon.activities.OverlayActivity"
          android:theme="@android:style/Theme.Translucent"
          android:configChanges="orientation|keyboardHidden|screenSize" />

<activity android:name="com.startapp.android.publish.adsCommon.activities.FullScreenActivity"
          android:theme="@android:style/Theme"
          android:configChanges="orientation|keyboardHidden|screenSize" />
<!--И сервисы-->
<service android:name="com.startapp.android.publish.common.metaData.PeriodicMetaDataService" />
<service android:name="com.startapp.android.publish.common.metaData.InfoEventService" />
<service android:name="com.startapp.android.publish.common.metaData.PeriodicJobService"
         android:permission="android.permission.BIND_JOB_SERVICE" />
<receiver android:name="com.startapp.android.publish.common.metaData.BootCompleteListener" >
	<intent-filter>
		<action android:name="android.intent.action.BOOT_COMPLETED" />
	</intent-filter>
</receiver>
```

### Официальная документация [StartApp SDK](https://github.com/StartApp-SDK/Documentation/wiki/Android-InApp-Documentation)

License
----
Apache-2.0

