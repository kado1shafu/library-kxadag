/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.app.Activity


interface ChartboostInterface {

    fun showChartboostVideo(isReward: Boolean)
    fun showChartboostVideo(location: String, isReward: Boolean)

    fun setChartboostLocation(location: String)
    fun setChartboostEventListener(listener: AdvertisementEventListener?)

    fun onCreate(activity: Activity)
    fun onResume(activity: Activity)
    fun onStart(activity: Activity)
    fun onPause(activity: Activity)
    fun onStop(activity: Activity)
    fun onDestroy(activity: Activity)
    fun onBackPressed(): Boolean
}