/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.app.Activity
import android.util.Log
import com.chartboost.sdk.CBLocation
import com.chartboost.sdk.Chartboost
import com.chartboost.sdk.ChartboostDelegate
import com.chartboost.sdk.Libraries.CBLogging
import com.chartboost.sdk.Model.CBError
import javax.inject.Inject

class ChartboostVideoManager @Inject constructor() : BaseVideoManager() {
    private val TAG = "ChartboostVideoManager"

    private var isVerified = false

    private var defaultCBLocation: String = CBLocation.LOCATION_HOME_SCREEN

    private val defaultDelegate = object : ChartboostDelegate() {

        // Implement to be notified of when a video will be displayed on the screen for a given CBLocation.
        // You can then do things like mute effects and sounds.
        override fun willDisplayVideo(location: String?) {
            Log.d(TAG, "[willDisplayVideo] $location")
        }

        //Called after the SDK has been successfully initialized and video prefetching has been completed.
        override fun didInitialize() {
            Log.d(TAG, "[didInitialize] Chartboost SDK is initialized and ready!")
        }

//----------------------------------------------------------------------------------------------------------------------

        // Called after a video has attempted to load from the Chartboost API servers but failed.
        override fun didFailToLoadRewardedVideo(location: String?, error: CBError.CBImpressionError?) {
            Log.w(TAG, "[didFailToLoadRewardedVideo] $location, error: $error")
            adEventListener?.onVideoNotLoaded(error.toString(), true)
        }

        override fun didFailToLoadInterstitial(location: String?, error: CBError.CBImpressionError?) {
            Log.w(TAG, "[didFailToLoadInterstitial] $location, error: $error")
            adEventListener?.onVideoNotLoaded(error.toString(), false)
        }


        // Called after a video has been displayed on the screen.
        override fun didDisplayRewardedVideo(location: String?) {
            Log.d(TAG, "[didDisplayRewardedVideo] $location")
            adEventListener?.onVideoOpened(true)
        }
        override fun didDisplayInterstitial(location: String?) {
            Log.d(TAG, "[didDisplayInterstitial] $location")
            adEventListener?.onVideoOpened(false)
        }

        // Called after a video has been loaded from the Chartboost API servers and cached locally.
        override fun didCacheInterstitial(location: String?) {
            Log.d(TAG, "[didCacheInterstitial] $location")
            adEventListener?.onVideoLoaded(false)
        }
        override fun didCacheRewardedVideo(location: String?) {
            Log.d(TAG, "[didCacheRewardedVideo] $location")
            adEventListener?.onVideoLoaded(true)
        }

        // Called after a video has been closed.
        override fun didCloseRewardedVideo(location: String?) {
            Log.d(TAG, "[didCloseRewardedVideo] $location")
            adEventListener?.onVideoClosed(isVerified)
            isVerified = false
            if (hasAutoLoad) loadVideo(location ?: defaultCBLocation, true)
        }
        override fun didCloseInterstitial(location: String?) {
            super.didCloseInterstitial(location)
            Log.d(TAG, "[didCloseInterstitial] $location")
            adEventListener?.onVideoClosed(false)
            if (hasAutoLoad) loadVideo(location ?: defaultCBLocation, false)
        }

        // Called after a rewarded video has been viewed completely and user is eligible for reward.
        override fun didCompleteRewardedVideo(location: String?, reward: Int) {
            Log.d(TAG, "[didCompleteRewardedVideo] $location")
            isVerified = true
            adEventListener?.rewardUser()
        }
    }

    init {
        Log.d(TAG, "Init")
    }

    fun setupSdk(
        activity: Activity,
        APP_ID: String,
        APP_SIGNATURE: String,
        levelLogging: CBLogging.Level = CBLogging.Level.NONE
    ) {
        Chartboost.startWithAppId(activity, APP_ID, APP_SIGNATURE)
        Chartboost.setLoggingLevel(levelLogging)

        //Call the Chartboost.setDelegate method before calling Chartboost.onCreate() during initialization. (c) CB Docs
        Chartboost.setDelegate(defaultDelegate)
    }

    fun setDefaultLocation(location: String) {
        defaultCBLocation = location
    }

    fun loadVideo(location: String = defaultCBLocation, isReward: Boolean = isVideoRewardDefault) {
        if (!isReadyToDisplay(isReward)) {
            adEventListener?.videoStartedLoading(isReward)
            if (isReward) {
                Log.d(TAG, "[loadVideo] Load rewarded video.")
                Chartboost.cacheRewardedVideo(location)
            } else {
                Log.d(TAG, "[loadVideo] Load interstitial video.")
                Chartboost.cacheInterstitial(location)
            }
        }
    }

    /**
     * If you intend to use interstitial and rewarding ads together,
     * then each time you call [showVideo] you need to specify the @param [isReward]
     */
    fun showVideo(location: String = defaultCBLocation, isReward: Boolean = isVideoRewardDefault) {
        if (isReadyToDisplay(isReward)) {
            if (isReward) {
                isVerified = false
                Chartboost.showRewardedVideo(location)
            } else {
                Chartboost.showInterstitial(location)
            }
        } else {
            Log.d(TAG, "Ad is not loaded.")
            adEventListener?.videoIsNotReady(isReward)
        }
    }

    fun isReadyToDisplay(location: String, isReward: Boolean): Boolean =
        if (isReward) Chartboost.hasRewardedVideo(location)
        else Chartboost.hasInterstitial(location)

    override fun isReadyToDisplay(isReward: Boolean): Boolean = isReadyToDisplay(defaultCBLocation, isReward)

    override fun setUserId(userID: String) = Chartboost.setCustomId(userID)
}
