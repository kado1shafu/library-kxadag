# Module Chartboost


[ ![Download](https://api.bintray.com/packages/kado1shafu/maven/kxadag-module-chartboost/images/download.svg) ](https://bintray.com/kado1shafu/maven/kxadag-module-chartboost/_latestVersion)

Chatboost SDK version: 7.3.1


## Integration

Подключить репозиторий Maven и добавить зависимость в gradle

```groovy
allprojects {
    repositories {
        maven {
            url 'https://dl.bintray.com/kado1shafu/maven'
        }
    }
}

...

dependencies {
    implementation "com.kado.one.shafu.kxadag:kxadag-module-chartboost:latestVersion"
}
```

Зайти на официальную документацию по Chartboost [How to Integrate](https://answers.chartboost.com/en-us/child_article/android), пропустить шаг загрузки SDK и выполнить все оставшиеся шаги по интеграции SDK.

License
----
Apache-2.0

