/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag


import android.app.Activity
import android.content.Context
import android.util.Log
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import javax.inject.Inject

class AdMobVideoManager @Inject constructor() : BaseVideoManager() {
    private val TAG = "AdMobVideoManager"

    private var isVerified = false

    private var APP_ID = ""
    private var REWARDED_ID = ""
    private var UNIT_ID = ""
    private var rewardedVideo: RewardedVideoAd? = null
    private var interstitialAd: InterstitialAd? = null

    private val interstitialRequest: AdRequest get() = AdRequest.Builder().build()
    private val rewardedRequest: AdRequest get() = AdRequest.Builder().build()

    private lateinit var defaultInterstitialListener: AdListener
    private lateinit var defaultRewardListener: RewardedVideoAdListener

    init {
        Log.i(TAG, "Init")
    }


    /**
     * Make all calls to the Mobile Ads SDK on the main thread. (c) Google
     * @param [context] - activity context.
     */
    fun initSdk(context: Context, appId: String, rewardedId: String = "", unitId: String = "") {
        MobileAds.initialize(context, appId)
        this.APP_ID = appId
        this.UNIT_ID = unitId
        this.REWARDED_ID = rewardedId
    }

    /**
     * @param [context] - activity context.
     */
    fun useInterstitialAndReward(context: Context) {
        useRewardVideo(context)
        useInterstitialVideo(context)
    }

    /**
     * @param [context] - activity context.
     */
    fun useInterstitialVideo(context: Context) {
        super.setInterstitialVideoAsDefault()
        defaultInterstitialListener = createInterstitialListener()

        interstitialAd = InterstitialAd(context)
        interstitialAd?.adUnitId = UNIT_ID
        interstitialAd?.adListener = defaultInterstitialListener
    }

    /**
     * @param [context] - activity context.
     */
    fun useRewardVideo(context: Context) {
        super.setRewardVideoAsDefault()
        defaultRewardListener = createRewardListener()
        rewardedVideo = MobileAds.getRewardedVideoAdInstance(context)
        rewardedVideo?.rewardedVideoAdListener = defaultRewardListener
    }

    fun loadVideo(isReward: Boolean = isVideoRewardDefault, rewardedId: String = REWARDED_ID) {
        if (!isReadyToDisplay(isReward)) {
            if (isReward) {
                Log.d(TAG, "[loadVideo] Load rewarded video")
                adEventListener?.videoStartedLoading(isReward)
                rewardedVideo?.loadAd(rewardedId, rewardedRequest)
            } else {
                Log.d(TAG, "[loadVideo] Load interstitial video")
                adEventListener?.videoStartedLoading(isReward)
                interstitialAd?.loadAd(interstitialRequest)
            }
        }
    }

    /**
     * If you intend to use interstitial and rewarding ads together,
     * then each time you call [showVideo] you need to specify the @param [isReward]
     */
    fun showVideo(isReward: Boolean = isVideoRewardDefault) {
        if (isReadyToDisplay(isReward)) {
            if (isReward) {
                isVerified = false
                rewardedVideo?.show()
            } else {
                interstitialAd?.show()
            }
        } else {
            Log.w(TAG, "[showVideo] Video is not ready")
            adEventListener?.videoIsNotReady(isReward)
        }
    }

    fun onPause(activity: Activity) = rewardedVideo?.pause(activity)
    fun onResume(activity: Activity) = rewardedVideo?.resume(activity)
    fun onDestroy(activity: Activity) = rewardedVideo?.destroy(activity)

    override fun isReadyToDisplay(isReward: Boolean): Boolean =
            if (isReward) rewardedVideo?.isLoaded ?: false
            else interstitialAd?.isLoaded ?: false

    override fun setUserId(userID: String) {}

    private fun createInterstitialListener() = object : AdListener() {
        override fun onAdLoaded() {
            Log.d(TAG, "[onAdLoaded]  AdMob ad loaded")
            adEventListener?.onVideoLoaded(false)
        }

        override fun onAdFailedToLoad(errorCode: Int) {
            Log.w(TAG, "[onAdFailedToLoad] Interstitial video failed to load with error code $errorCode")
            adEventListener?.onVideoNotLoaded("Error code: $errorCode", false)
        }

        override fun onAdOpened() {
            Log.d(TAG, "[onAdOpened] Ad opened")
            adEventListener?.onVideoOpened(false)
        }

        override fun onAdClosed() {
            Log.d(TAG, "[onAdClosed] Ad closed")
            adEventListener?.onVideoClosed(false)
            if (hasAutoLoad) loadVideo(false)
        }
    }

    private fun createRewardListener() = object : RewardedVideoAdListener {

        //Called when a rewarded video ad starts to play.
        override fun onRewardedVideoStarted() {
            Log.d(TAG, "[onRewardedVideoStarted] Ad started")
        }

        //Called when a rewarded video ad completes playing.
        override fun onRewardedVideoCompleted() {
            Log.d(TAG, "[onRewardedVideoCompleted] Ad ended")
        }

        //Called when a rewarded video ad leaves the application (e.g., to go to the browser).
        override fun onRewardedVideoAdLeftApplication() {
            Log.d(TAG, "[onRewardedVideoAdLeftApplication] Left application")
        }
//----------------------------------------------------------------------------------------------------------------------


        //Called when a rewarded video ad has triggered a reward.
        override fun onRewarded(rewardItem: RewardItem) {
            isVerified = true
            adEventListener?.rewardUser()
        }

        //Called when a rewarded video ad is loaded.
        override fun onRewardedVideoAdLoaded() {
            Log.d(TAG, "[onRewardedVideoAdLoaded]  AdMob ad loaded")
            adEventListener?.onVideoLoaded(true)
        }

        //Called when a rewarded video ad request failed.
        override fun onRewardedVideoAdFailedToLoad(errorCode: Int) {
            Log.w(TAG, "[onRewardedVideoAdFailedToLoad] Rewarded video failed to load with error code $errorCode")
            adEventListener?.onVideoNotLoaded("Error code: $errorCode", true)
        }

        //Called when a rewarded video ad opens a overlay that covers the screen.
        override fun onRewardedVideoAdOpened() {
            Log.d(TAG, "[onRewardedVideoAdOpened] Ad opened")
            adEventListener?.onVideoOpened(true)
        }

        //Called when a rewarded video ad is closed.
        override fun onRewardedVideoAdClosed() {
            Log.d(TAG, "[onRewardedVideoAdClosed] Ad Dismissed")
            adEventListener?.onVideoClosed(isVerified)
            isVerified = false
            if (hasAutoLoad) loadVideo(true)
        }
    }
}
