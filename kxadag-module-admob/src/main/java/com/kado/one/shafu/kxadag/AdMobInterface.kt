package com.kado.one.shafu.kxadag

interface AdMobInterface {

    fun showAdMobVideo(isReward: Boolean)
    fun setAdMobEventListener(listener: AdvertisementEventListener?)
}