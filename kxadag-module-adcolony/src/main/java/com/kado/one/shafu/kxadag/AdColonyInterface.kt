/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

interface AdColonyInterface {
    /**
     * Showing Interstitial or Rewarded Interstitial Ads
     */
    fun showAdColonyVideo(isReward: Boolean)

    fun setAdColonyEventListener(listener: AdvertisementEventListener?)
}