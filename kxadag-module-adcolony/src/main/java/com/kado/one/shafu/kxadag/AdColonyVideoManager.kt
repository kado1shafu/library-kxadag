/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.app.Activity
import android.util.Log
import com.adcolony.sdk.*
import javax.inject.Inject


class AdColonyVideoManager @Inject constructor() : BaseVideoManager() {
    private val TAG = "AdColonyVideoManager"

    private var adVideo: AdColonyInterstitial? = null
    private var zoneId: String = ""
    private var isVerified = false

    private val defaultRewardListener: AdColonyRewardListener = AdColonyRewardListener {
        // Query reward object for info here
        Log.d(TAG, "[onReward] $isVerified")
        isVerified = true
        adEventListener?.rewardUser()
    }

    private val defaultInterstitialListener: AdColonyInterstitialListener = object : AdColonyInterstitialListener() {

        override fun onExpiring(ad: AdColonyInterstitial?) {
            // Request a new ad if ad is expiring
            Log.d(TAG, "[onExpiring] Ad expiring")
        }
//----------------------------------------------------------------------------------------------------------------------

        // Ad passed back in request filled callback, ad can now be shown
        override fun onRequestFilled(ad: AdColonyInterstitial?) {
            Log.d(TAG, "[onRequestFilled] AdColony ad loaded")
            adVideo = ad
            adEventListener?.onVideoLoaded(isVideoRewardDefault)
        }

        // Ad request was not filled
        override fun onRequestNotFilled(zone: AdColonyZone?) {
            Log.w(TAG, "[onRequestNotFilled] Load AdColony ad failed, zoneId: ${zone?.zoneID}")
            adEventListener?.onVideoNotLoaded("Load AdColony ad failed, zoneId: ${zone?.zoneID}", isVideoRewardDefault)
        }

        override fun onOpened(ad: AdColonyInterstitial?) {
            Log.d(TAG, "[onOpened] Ad opened")
            adEventListener?.onVideoOpened(isVideoRewardDefault)
        }

        override fun onClosed(ad: AdColonyInterstitial?) {
            Log.d(TAG, "[onClosed] Ad closed")
            adVideo = null
            adEventListener?.onVideoClosed(isVerified)
            isVerified = false

            if (hasAutoLoad) loadVideo()
        }
    }


    init {
        Log.i(TAG, "Init")
    }

    // Construct optional [appOptions] object to be sent with configure
    fun initSdk(
        activity: Activity,
        appId: String,
        zoneId: String,
        appOptions: AdColonyAppOptions = AdColonyAppOptions()
    ) {
        // Configure AdColony in your launching Activity's onCreate() method so that cached ads can
        // be available as soon as possible.
        AdColony.configure(activity, appOptions, appId, zoneId)
        this.zoneId = zoneId
    }


    fun loadVideo(isReward: Boolean = isVideoRewardDefault) {
        if (!isReadyToDisplay(isReward)) {
            Log.d(TAG, "[loadVideo] Load video")
            adEventListener?.videoStartedLoading(isReward)
            adVideo = null
            AdColony.requestInterstitial(zoneId, defaultInterstitialListener)
        }
    }

    fun showVideo(isReward: Boolean = isVideoRewardDefault) {
        if (isReadyToDisplay(isReward)) {
            isVerified = false
            adVideo?.show()
        } else {
            Log.w(TAG, "[showVideo] Video is not ready")
            adEventListener?.videoIsNotReady(isReward)
        }
    }


    override fun setRewardVideoAsDefault() {
        super.setRewardVideoAsDefault()
        AdColony.setRewardListener(defaultRewardListener)
    }

    override fun setInterstitialVideoAsDefault() {
        super.setInterstitialVideoAsDefault()
        AdColony.removeRewardListener()
    }

    override fun isReadyToDisplay(isReward: Boolean): Boolean {
        val ad = adVideo
        return ad != null && !ad.isExpired
    }

    override fun setUserId(userID: String) {
        /** Get current AdColonyAppOptions and change user id */
        val appOptions = AdColony.getAppOptions().setUserID(userID)

        /** Send new information to AdColony */
        AdColony.setAppOptions(appOptions)
    }

}