/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag


enum class TypeAd(val type: Int) {
    ADMOB(1), APPLOVIN(2), CHARTBOOST(3), ADCOLONY(4), STARTAPP(5), VUNGLE(6)
}