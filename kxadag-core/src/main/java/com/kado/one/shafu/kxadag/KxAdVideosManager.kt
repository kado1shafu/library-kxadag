/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.app.Activity
import android.content.Context

interface KxAdVideosManager {
    fun initSdks(activity: Activity)

    fun configure(context: Context)

    fun preloadRewardedVideos()

    fun showRewardedVideo(typeAd: TypeAd) {
        showRewardedVideo(typeAd.type)
    }

    fun showRewardedVideo(typeAdId: Int)
}