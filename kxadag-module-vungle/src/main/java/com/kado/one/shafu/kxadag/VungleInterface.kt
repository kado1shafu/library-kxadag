/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag


interface VungleInterface {

    fun showVungleVideo()
    fun showVungleVideo(placementId: String)
    fun setVungleEventListener(listener: AdvertisementEventListener?)
}