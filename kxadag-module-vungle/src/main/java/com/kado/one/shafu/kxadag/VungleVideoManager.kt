/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.content.Context
import android.util.Log
import com.vungle.warren.*
import javax.inject.Inject

class VungleVideoManager @Inject constructor() : BaseVideoManager() {

    private val TAG = "VungleVideoManager"

    private lateinit var defaultConfig: AdConfig

    private var defaultPlacementId = ""

    private val defaultLoadingListener: LoadAdCallback = object : LoadAdCallback {
        override fun onAdLoad(placementReferenceId: String?) {
            Log.d(TAG, "[onAdLoad]  Vungle ad loaded")
            adEventListener?.onVideoLoaded(isVideoRewardDefault)
        }

        override fun onError(placementReferenceId: String?, error: Throwable?) {
            Log.w(TAG, "[onError] Ad failed to load. PRID: $placementReferenceId Error: ${error?.localizedMessage}")
            adEventListener?.onVideoNotLoaded(
                "[onError] Ad failed to load. PRID: $placementReferenceId Error: ${error?.localizedMessage}",
                isVideoRewardDefault
            )
        }

    }

    private val defaultDisplayListener: PlayAdCallback = object : PlayAdCallback {
        // Placement reference ID for the placement that has completed ad experience
        // completed has value of true or false to notify whether video was
        // watched for 80% or more
        // isCTAClkcked has value of true or false to indicate whether download button
        // of an ad has been clicked by the user
        override fun onAdEnd(placementReferenceId: String?, completed: Boolean, isCTAClicked: Boolean) {
            if (completed && isVideoRewardDefault) adEventListener?.rewardUser()
            Log.d(TAG, "[adHidden] Ad Dismissed")
            adEventListener?.onVideoClosed(false)
            if (hasAutoLoad) loadVideo()
        }

        override fun onAdStart(placementReferenceId: String?) {
            Log.d(TAG, "[onAdStart] Ad opened")
            adEventListener?.onVideoOpened(isVideoRewardDefault)
        }

        override fun onError(placementReferenceId: String?, error: Throwable?) {
            Log.w(TAG, "[onError] Play ad error occurred - ${error?.localizedMessage}, for PRID $placementReferenceId")
            adEventListener?.videoNotDisplayed(
                "Play ad error occurred - ${error?.localizedMessage}",
                isVideoRewardDefault
            )
        }
    }

    init {
        Log.d(TAG, "Init")
    }

    fun setupSdk(applicationContext: Context, appId: String, placementId: String = "") {
        Vungle.init(appId, applicationContext, object : InitCallback {
            override fun onSuccess() {
                Log.i(TAG, "Initialization Vungle SDK has succeeded")
                // Initialization has succeeded and SDK is ready to load an ad or play one if there
                // is one pre-cached already
            }

            override fun onAutoCacheAdAvailable(placementId: String?) {
                Log.i(TAG, "AutoCacheAdAvailable for  $placementId")
                // Callback to notify when an ad becomes available for the auto-cached placement
                // NOTE: This callback works only for the auto-cached placement. Otherwise, please use
                // LoadAdCallback with loadAd API for loading placements.
            }

            override fun onError(error: Throwable?) {
                Log.e(TAG, "Initialization error occurred: ${error?.localizedMessage}")
                // Initialization error occurred - throwable.getLocalizedMessage() contains error message
            }

        })
        defaultPlacementId = placementId
        defaultConfig = AdConfig().apply {
            setAutoRotate(true)
            setBackButtonImmediatelyEnabled(false)
            setMuted(true)
        }
    }

    fun configure(config: AdConfig) {
        defaultConfig = config
    }

    override fun setRewardVideoAsDefault() {
        super.setRewardVideoAsDefault()
        // Optional settings for rewarded ads
        Vungle.setIncentivizedFields(null, null, null, null, null)
    }

    fun loadVideo(placementId: String = defaultPlacementId) {
        if (!isReadyToDisplay(placementId)) {
            Log.d(TAG, "[loadVideo] Load ad video")
            adEventListener?.videoStartedLoading(isVideoRewardDefault)
            Vungle.loadAd(placementId, defaultLoadingListener)
        }
    }

    fun showVideo(placementId: String = defaultPlacementId) {
        if (isReadyToDisplay(placementId)) {
            Vungle.playAd(placementId, defaultConfig, defaultDisplayListener)
        } else {
            Log.w(TAG, "[showVideo] Video is not ready")
            adEventListener?.videoIsNotReady(isVideoRewardDefault)
        }
    }

    fun isReadyToDisplay(placement: String) = Vungle.canPlayAd(placement)

    override fun isReadyToDisplay(isReward: Boolean): Boolean = isReadyToDisplay(defaultPlacementId)

    override fun setUserId(userID: String) {
        if (isVideoRewardDefault) Vungle.setIncentivizedFields(userID, null, null, null, null)
    }
}