/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.app.Activity
import android.content.Context
import com.chartboost.sdk.CBLocation
import com.chartboost.sdk.Chartboost
import javax.inject.Inject


class VideoManager @Inject constructor(
    private val adColonyManager: AdColonyVideoManager,
    private val chartboostManager: ChartboostVideoManager,
    private val adMobManager: AdMobVideoManager,
    private val appLovinManager: AppLovinVideoManager,
    private val startAppManager: StartAppVideoManager,
    private val vungleManager: VungleVideoManager
) : KxAdVideosManager, AdColonyInterface, AdMobInterface, AppLovinInterface, ChartboostInterface, StartAppInterface,
    VungleInterface {

    override fun initSdks(activity: Activity) {
        adColonyManager.initSdk(activity, BuildConfig.ADCOLONY_APP_ID, BuildConfig.ADCOLONY_ZONE_ID)
        chartboostManager.setupSdk(activity, BuildConfig.CHARTBOOST_APP_ID, BuildConfig.CHARTBOOST_APP_SIGNATURE)
        adMobManager.initSdk(activity, BuildConfig.ADMOB_APP_ID, rewardedId = BuildConfig.ADMOB_REWARDED_ID)
        appLovinManager.setupSdk(activity.applicationContext, BuildConfig.APPLOVIN_SDK_KEY)
        startAppManager.setupSdk(activity, BuildConfig.STARTAPP_APP_ID)
        vungleManager.setupSdk(activity.applicationContext, BuildConfig.VUNGLE_APP_ID, BuildConfig.VUNGLE_PLACEMENT_ID)
    }

    override fun configure(context: Context) {
        chartboostManager.apply {
            setDefaultLocation(CBLocation.LOCATION_HOME_SCREEN)
            setRewardVideoAsDefault()
            setAutoLoad(true)
        }

        adColonyManager.apply {
            setRewardVideoAsDefault()
            setAutoLoad(true)
        }

        adMobManager.apply {
            useRewardVideo(context)
            setAutoLoad(true)
        }
        appLovinManager.apply {
            useRewardVideo(context)
            setAutoLoad(true)
        }

        startAppManager.apply {
            userRewardedVideo(context)
            setAutoLoad(true)
        }
        vungleManager.apply {
            setRewardVideoAsDefault()
            setAutoLoad(true)
        }
    }

    override fun preloadRewardedVideos() {
        adColonyManager.loadVideo()
        chartboostManager.loadVideo()
    }

    override fun showRewardedVideo(typeAdId: Int) {
        when (typeAdId) {
            TypeAd.ADMOB.type -> showAdMobVideo(true)
            TypeAd.ADCOLONY.type -> showAdColonyVideo(true)
            TypeAd.CHARTBOOST.type -> showChartboostVideo(true)
            TypeAd.STARTAPP.type -> showStartAppVideo(true)
            TypeAd.VUNGLE.type -> showStartAppVideo(true)
        }
    }

    override fun showVungleVideo() = vungleManager.showVideo()
    override fun showAdMobVideo(isReward: Boolean) = adMobManager.showVideo(isReward)
    override fun showAdColonyVideo(isReward: Boolean) = adColonyManager.showVideo(isReward)
    override fun showStartAppVideo(isReward: Boolean) = startAppManager.showVideo(isReward)
    override fun showChartboostVideo(isReward: Boolean) = chartboostManager.showVideo(isReward = isReward)
    override fun showAppLovinVideo(context: Context, isReward: Boolean) =
        appLovinManager.showVideo(context, isReward)


    override fun setAdMobEventListener(listener: AdvertisementEventListener?) =
        adMobManager.setEventListener(listener)

    override fun setVungleEventListener(listener: AdvertisementEventListener?) =
        vungleManager.setEventListener(listener)

    override fun setAppLovinEventListener(listener: AdvertisementEventListener?) =
        appLovinManager.setEventListener(listener)

    override fun setAdColonyEventListener(listener: AdvertisementEventListener?) =
        adColonyManager.setEventListener(listener)

    override fun setStartAppEventListener(listener: AdvertisementEventListener?) =
        startAppManager.setEventListener(listener)

    override fun setChartboostEventListener(listener: AdvertisementEventListener?) =
        chartboostManager.setEventListener(listener)


    override fun setChartboostLocation(location: String) {
        chartboostManager.setDefaultLocation(location)
    }


//----------------------------------------------------------------------------------------------------------------------

    override fun showChartboostVideo(location: String, isReward: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showVungleVideo(placementId: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(activity: Activity) {
        Chartboost.onCreate(activity)
    }

    override fun onResume(activity: Activity) {
        Chartboost.onResume(activity)
        adMobManager.onResume(activity)
    }

    override fun onStart(activity: Activity) {
        Chartboost.onStart(activity)
    }

    override fun onPause(activity: Activity) {
        Chartboost.onPause(activity)
        adMobManager.onPause(activity)
    }

    override fun onStop(activity: Activity) {
        Chartboost.onStop(activity)
    }

    override fun onDestroy(activity: Activity) {
        Chartboost.onDestroy(activity)
        adMobManager.onDestroy(activity)
    }

    override fun onBackPressed(): Boolean {
        return Chartboost.onBackPressed()
    }

}