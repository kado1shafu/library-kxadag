/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.content.Context
import toothpick.config.Module

class AppModule(context: Context) : Module() {

    init {
        bind(Context::class.java).toInstance(context)

        bind(AdColonyVideoManager::class.java).toInstance(AdColonyVideoManager())
        bind(ChartboostVideoManager::class.java).toInstance(ChartboostVideoManager())
        bind(VideoManager::class.java).singletonInScope()
    }
}