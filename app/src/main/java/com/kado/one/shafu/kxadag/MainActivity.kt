/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import toothpick.Scope
import toothpick.Toothpick

class MainActivity : AppCompatActivity() {

    private val scope: Scope = Toothpick.openScope(DI.APP_SCOPE)

    private lateinit var videoManager: VideoManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        videoManager = scope.getInstance(VideoManager::class.java)
        videoManager.initSdks(this)

        setContentView(R.layout.activity_main)
        // Очень важно вызвать videoManager.configure до videoManager.onCreate, см реализацию.
        videoManager.configure(this)
        videoManager.onCreate(this)

        videoManager.preloadRewardedVideos()
    }

    override fun onResume() {
        super.onResume()
        videoManager.onResume(this)

    }

    override fun onStart() {
        super.onStart()
        videoManager.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        videoManager.onPause(this)
    }

    override fun onStop() {
        super.onStop()
        videoManager.onStop(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        videoManager.onDestroy(this)
    }

    override fun onBackPressed() {
        if (videoManager.onBackPressed()) return
        else super.onBackPressed()
    }

}

