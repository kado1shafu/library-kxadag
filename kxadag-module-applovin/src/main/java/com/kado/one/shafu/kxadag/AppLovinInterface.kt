/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.content.Context

interface AppLovinInterface {

    fun showAppLovinVideo(context: Context, isReward: Boolean)

    fun setAppLovinEventListener(listener: AdvertisementEventListener?)
}