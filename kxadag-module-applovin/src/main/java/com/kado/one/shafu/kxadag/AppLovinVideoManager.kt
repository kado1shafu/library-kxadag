/*
 * Developed by Vitaly Nechaev.
 * Copyright (c) 2019. All rights reserved.
 */

package com.kado.one.shafu.kxadag

import android.content.Context
import android.util.Log
import com.applovin.adview.AppLovinIncentivizedInterstitial
import com.applovin.adview.AppLovinInterstitialAd
import com.applovin.adview.AppLovinInterstitialAdDialog
import com.applovin.sdk.*
import javax.inject.Inject

class AppLovinVideoManager @Inject constructor() : BaseVideoManager() {
    private val TAG = "AppLovinVideoManager"

    private var interstitialZoneId = ""
    private var interstitialVideoAd: AppLovinAd? = null
    private var interstitialAdIsAdReadyToDisplay = false
    private var interstitialVideoInstance: AppLovinInterstitialAdDialog? = null
    private lateinit var defaultInterstitialDisplayListener: AppLovinAdDisplayListener
    private lateinit var defaultInterstitialLoadListener: AppLovinAdLoadListener

    private var isVerified = false
    private var rewardedVideoInstance: AppLovinIncentivizedInterstitial? = null
    private lateinit var defaultRewardDisplayListener: AppLovinAdDisplayListener
    private lateinit var defaultRewardLoadListener: AppLovinAdLoadListener
    private lateinit var defaultRewardListener: AppLovinAdRewardListener

    private val defaultPlaybackListener = object : AppLovinAdVideoPlaybackListener {
        override fun videoPlaybackBegan(appLovinAd: AppLovinAd) {
            Log.d(TAG, "[videoPlaybackBegan] Ad video began")
        }

        override fun videoPlaybackEnded(appLovinAd: AppLovinAd, percentViewed: Double, fullyWatched: Boolean) {
            Log.d(TAG, "[videoPlaybackBegan] Ad video ended, viewed:$percentViewed%, isFullyWatched $fullyWatched ")
        }
    }

    private lateinit var appLovinSdk: AppLovinSdk

    init {
        //See more -> https://github.com/AppLovin/Android-SDK-Demo
        Log.d(TAG, "Init")
    }

    //Initializing our SDK at launch is very important as it'll start pre-loading ads in the background.
    fun setupSdk(applicationContext: Context, sdkKey: String, isTestAdsEnabled: Boolean = false) {
        AppLovinSdk.initializeSdk(applicationContext)
        val sdk = AppLovinSdk.getInstance(applicationContext).apply {
            settings.isTestAdsEnabled = isTestAdsEnabled
        }

        checkSdkKey(sdkKey, sdk.sdkKey)
        appLovinSdk = sdk
    }

    /**
     * @param [applicationContext] - application context.
     */
    fun useInterstitialVideo(applicationContext: Context, zoneId: String = "") {
        super.setInterstitialVideoAsDefault()
        interstitialZoneId = zoneId
        defaultInterstitialDisplayListener = createInterstitialDisplayListener()
        defaultInterstitialLoadListener = createInterstitialLoadListener()

        val instance = AppLovinInterstitialAd.create(appLovinSdk, applicationContext).apply {
            setAdDisplayListener(defaultInterstitialDisplayListener)
            setAdLoadListener(defaultInterstitialLoadListener)
            setAdVideoPlaybackListener(defaultPlaybackListener)
        }

        interstitialVideoInstance = instance
    }

    /**
     * @param [applicationContext] - application context.
     */
    fun useRewardVideo(applicationContext: Context, zoneId: String = "") {
        super.setRewardVideoAsDefault()
        rewardedVideoInstance = if (zoneId.isNotEmpty())
            AppLovinIncentivizedInterstitial.create(zoneId, appLovinSdk)
        else
            AppLovinIncentivizedInterstitial.create(applicationContext)

        defaultRewardDisplayListener = createRewardDisplayListener()
        defaultRewardLoadListener = createRewardLoadListener()
        defaultRewardListener = createRewardListener()
    }


    fun loadVideo(isReward: Boolean = isVideoRewardDefault) {
        if (!isReadyToDisplay(isReward)) {
            if (isReward) {
                Log.d(TAG, "[loadVideo] Load rewarded video")
                adEventListener?.videoStartedLoading(isReward)
                rewardedVideoInstance?.preload(defaultRewardLoadListener)
            } else {
                Log.d(TAG, "[loadVideo] Load interstitial video")
                adEventListener?.videoStartedLoading(isReward)
                if (interstitialZoneId.isEmpty())
                    appLovinSdk.adService.loadNextAd(AppLovinAdSize.INTERSTITIAL, defaultInterstitialLoadListener)
                else
                    appLovinSdk.adService.loadNextAdForZoneId(interstitialZoneId, defaultInterstitialLoadListener)
            }
        }
    }

    fun showVideo(context: Context, isReward: Boolean = isVideoRewardDefault) {
        if (isReadyToDisplay(isReward)) {
            if (isReward) {
                isVerified = false
                rewardedVideoInstance?.show(
                    context,
                    defaultRewardListener,
                    defaultPlaybackListener,
                    defaultRewardDisplayListener
                )
            } else {
                interstitialVideoAd?.let { interstitialVideoInstance?.showAndRender(it) }

            }
        } else {
            Log.w(TAG, "[showVideo] Video is not ready")
            adEventListener?.videoIsNotReady(isReward)
        }
    }

    override fun isReadyToDisplay(isReward: Boolean): Boolean =
        if (isReward) rewardedVideoInstance?.isAdReadyToDisplay ?: false
        else interstitialAdIsAdReadyToDisplay

    //Set an optional user identifier used for S2S callbacks
    override fun setUserId(userID: String) {
        rewardedVideoInstance?.userIdentifier = userID
    }

    private fun createInterstitialDisplayListener() = object : AppLovinAdDisplayListener {
        override fun adDisplayed(appLovinAd: AppLovinAd) {
            Log.d(TAG, "[adDisplayed] Ad opened")
            AppLovinSdkUtils.runOnUiThread { adEventListener?.onVideoOpened(false) }
        }

        override fun adHidden(appLovinAd: AppLovinAd) {
            Log.d(TAG, "[adHidden] Ad Dismissed")
            AppLovinSdkUtils.runOnUiThread { adEventListener?.onVideoClosed(false) }
            interstitialVideoAd = null
            interstitialAdIsAdReadyToDisplay = false
            if (hasAutoLoad) loadVideo(false)
        }
    }

    private fun createInterstitialLoadListener() = object : AppLovinAdLoadListener {

        override fun adReceived(appLovinAd: AppLovinAd) {
            Log.d(TAG, "[adReceived]  AppLovin ad loaded")
            interstitialVideoAd = appLovinAd
            AppLovinSdkUtils.runOnUiThread { adEventListener?.onVideoLoaded(false) }
            interstitialAdIsAdReadyToDisplay = true
        }

        override fun failedToReceiveAd(errorCode: Int) {
            Log.w(TAG, "[failedToReceiveAd] Interstitial video failed to load with error code $errorCode")
            AppLovinSdkUtils.runOnUiThread { adEventListener?.onVideoNotLoaded("Error code: $errorCode", false) }
            interstitialAdIsAdReadyToDisplay = false
        }
    }

    private fun createRewardDisplayListener() = object : AppLovinAdDisplayListener {
        override fun adDisplayed(appLovinAd: AppLovinAd) {
            Log.d(TAG, "[adDisplayed] Ad opened")
            AppLovinSdkUtils.runOnUiThread { adEventListener?.onVideoOpened(true) }
        }

        override fun adHidden(appLovinAd: AppLovinAd) {
            Log.d(TAG, "[adHidden] Ad Dismissed")
            AppLovinSdkUtils.runOnUiThread { adEventListener?.onVideoClosed(isVerified) }
            isVerified = false
            if (hasAutoLoad) loadVideo(true)
        }
    }

    private fun createRewardLoadListener() = object : AppLovinAdLoadListener {

        override fun adReceived(appLovinAd: AppLovinAd) {
            Log.d(TAG, "[adReceived]  AppLovin ad loaded")
            AppLovinSdkUtils.runOnUiThread { adEventListener?.onVideoLoaded(true) }
        }

        override fun failedToReceiveAd(errorCode: Int) {
            Log.w(TAG, "[failedToReceiveAd] Rewarded video failed to load with error code $errorCode")
            AppLovinSdkUtils.runOnUiThread { adEventListener?.onVideoNotLoaded("Error code: $errorCode", true) }
        }
    }

    private fun createRewardListener() = object : AppLovinAdRewardListener {

        override fun userRewardVerified(appLovinAd: AppLovinAd, map: Map<String, String>) {
            // AppLovin servers validated the reward. Refresh user balance from your server.  We will also pass the number of coins
            // awarded and the name of the currency.  However, ideally, you should verify this with your server before granting it.
            isVerified = true
            adEventListener?.rewardUser()
            // By default we'll show a alert informing your user of the currency & amount earned.
            // If you don't want this, you can turn it off in the Manage Apps UI. (c) AppLovin support
        }

        override fun userOverQuota(appLovinAd: AppLovinAd, map: Map<String, String>) {
            // Your user has already earned the max amount you allowed for the day at this point, so
            // don't give them any more currency. By default we'll show them a alert explaining this,
            // though you can change that from the AppLovin dashboard.
            Log.w(TAG, "Reward validation request exceeded quota with response: $map")
        }

        override fun userRewardRejected(appLovinAd: AppLovinAd, map: Map<String, String>) {
            // Your user couldn't be granted a reward for this view. This could happen if you've blacklisted
            // them, for example. Don't grant them any currency. By default we'll show them an alert explaining this,
            // though you can change that from the AppLovin dashboard.
            Log.w(TAG, "Reward validation request was rejected with response: $map")
        }

        override fun validationRequestFailed(appLovinAd: AppLovinAd, responseCode: Int) {
            when (responseCode) {
                AppLovinErrorCodes.INCENTIVIZED_USER_CLOSED_VIDEO -> {
                    // Your user exited the video prematurely. It's up to you if you'd still like to grant
                    // a reward in this case. Most developers choose not to. Note that this case can occur
                    // after a reward was initially granted (since reward validation happens as soon as a
                    // video is launched).
                    Log.w(TAG, "Reward validation request failed with error INCENTIVIZED_USER_CLOSED_VIDEO")
                }
                AppLovinErrorCodes.INCENTIVIZED_SERVER_TIMEOUT, AppLovinErrorCodes.INCENTIVIZED_UNKNOWN_SERVER_ERROR -> {
                    // Some server issue happened here. Don't grant a reward. By default we'll show the user
                    // a alert telling them to try again later, but you can change this in the
                    // AppLovin dashboard.
                    Log.w(TAG, "Reward validation request failed with error INCENTIVIZED_UNKNOWN_SERVER_ERROR")
                }
                AppLovinErrorCodes.INCENTIVIZED_NO_AD_PRELOADED -> {
                    // Indicates that the developer called for a rewarded video before one was available.
                    // Note: This code is only possible when working with rewarded videos.
                    Log.w(TAG, "Reward validation request failed with error INCENTIVIZED_NO_AD_PRELOADED")
                }
                else -> Log.w(TAG, "Reward validation request failed with error code: $responseCode")
            }
        }

        override fun userDeclinedToViewAd(appLovinAd: AppLovinAd) {
            // This method will be invoked if the user selected "no" when asked if they want to view an ad.
            // If you've disabled the pre-video prompt in the "Manage Apps" UI on our website, then this method won't be called.
            Log.i(TAG, "User declined to view ad")
            isVerified = false
        }
    }

    private fun checkSdkKey(mySdkKey: String, sdkKey: String): Boolean {
        val checkState = mySdkKey.equals(sdkKey, ignoreCase = true)
        if (!checkState) {
            Log.e(TAG, "Please update your sdk key in the manifest file.")
        }
        return checkState
    }
}
